'use strict'
var Netcat = require('node-netcat');
var fs = require('fs');
let myArgs = process.argv.slice(2);
let cmd = myArgs[0];
let filename = myArgs[1];
var env = process.env.NODE_ENV || 'pavlovhorde';
var config = require('../config/config.js')[env];
var client = Netcat.client(config.rcon.port, config.rcon.hostip, {timeout: config.rcon.timeout} );
client.on('open', function () {
  client.send(config.rcon.password + '\n');
});

client.on('data', async function (data) {
  let temp = {};
  let result =  data.toString('ascii');
  console.log(result);
  console.log(data)
  if (result.slice(0,15) == 'Authenticated=1') {
    client.send(cmd + '\n')
    return {result: 'Authenticated'}
  } else if (result.slice(0,15) == 'Authenticated=0'){
    return {result: 'Authentication failure'}
  } else if (result.slice(0,9) == 'Password:') {
    return {result: 'Prompt for password'}
  } else if (result.slice(0,1) !== '{' || result.slice(0,1) !== '[') {
    try {
      await fs.writeFile('./tmp/' + filename , result, {encoding:'utf8',flag:'w'})
      client.send('Disconnect' | '\n', true);
      return result
    } catch (error) {
      console.log(error)
      return {msg: 'error on parse'}
    }
  } else {
    // I think the message on disconnect is Goodbye
    return {msg: 'dropped into else'}
  }
});
  
client.on('error', function (err) {
  console.log(err);
});
  
client.on('close', function () {
  //console.log('close');
});
client.start();