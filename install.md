!!!DRAFT!!!
Installation Instructions

Google Assistant Account creation and install:
1. Create actions on google account
2. Create project in actions on google console
3. Login to Dialogflow, click through acceptance.  (This will let actions on google launch into dialogflow)
4. Perform all the setup in actions on google.
!!! Remember - YOU are the user of the app.  Google's privacy policy, etc is for users of the app...  I also never take the app out of Alpha.  Google does a rigoroush check when you publish to production - which I never do.  While the app is in Alpha, you control who can invoke it :-), which is exactly the state we want to persist in. !!!
5. Build should take you into dialogflow.  (If it doesn't do this, that generally means you didn't do the click through in dialogflow yet.)  
6. There is a zip file Pavlov-Suncoast-Server.zip in the repo in the assistant folder, that is the zip file to import.
7. Go to fullfillment - if webhook is not enabled, enable it.  Change the url like to https://<hostname>:<port>/aog.  Where hostname/port are what you plan to use for the node server.
8. Even though we have no backend server, many of the "intents" have default response if the server is down.  So you can test some of the commands now.  Type in "server Status" for example.

Backend node server install and configuration:
1. Install node and npm
2. Git clone
3. npm install .        (install the server)
4. Update the config file
5. Setup certificates


Setup Dialogflow
* Tutorial if you prefer: https://codelabs.developers.google.com/codelabs/actions-1#0
* Go to fullfillment, enable webhook.  
https://dialogflow.cloud.google.com/#/agent/`project_name`/fulfillment
- flip switch to enable webhook
- Enter host/endpoint information - eg. https://`hostname:port`/aog
- Don't forget to scroll down and click SAVE
* Import google assistant artifacts (intents/entities)
* Before proceeding, play around in dialogflow driving the intents, seeing what happens when your server is down.

Setup certificates for node server.  
* Google assistant requires https.  If it's not working right, the errors are not obvious and are only seen on the "google" side.


