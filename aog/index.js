// include actions on google library
const {
  dialogflow,
  actionssdk,
  Image,
  Table,
  Carousel,
  List
} = require("actions-on-google");

const path = require('path');
const fs = require('fs');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
let env = process.env.NODE_ENV || 'pavlovhorde';
let config = require('../config/config.js')[env];
let uniqueId = '';

// perform RCON command
// I couldn't get netcat to work reliably via async, so this calls a node.js script which issues the
// RCON commands and writes the result to a file, which I then read in.  :-/
// I really wish I understood async/await enough to eliminate using files.
async function rconCmd(cmd, filename) {
  let respData = '';
  console.log('execute rconCmd: ' + cmd);
  const {stdout, stderr } = await exec(`node ./scripts/rconCmd.js ` + cmd + ` ` + filename);
  respData = await fs.readFileSync(`./tmp/` + filename);
  console.log(respData);
  if (respData.length > 0) {
    return JSON.parse(respData);
  } else
  {
    return respData;
  }
};

async function userListDetails() {
  class UserDetails {
    constructor(playerName, uniqueId, kills, deaths, assists, score, cash, teamId, teamColor ) {
      this.playerName = playerName;
      this.uniqueId = uniqueId;
      this.kills = kills
      this.deaths = deaths
      this.assists = assists
      this.score = score
      this.cash = cash
      this.teamId = teamId
      this.teamColor = teamColor
    }
  }
  usrListDetails = [];
  let tmpUserName = '';
  let cmdText = `"RefreshList"`;
  let filename = `refreshList.json`;
  const userInfo = await rconCmd (cmdText, filename);
  console.log(userInfo);
  if (userInfo.length == 0) {
    throw new Error("Sorry, RCON didn't respond in time, is there anything else I can help you with?");
  } else if (userInfo.PlayerList.length == 0) {
    throw new Error("RCON reports no players on server, is there anything else I can help you with?");
  } else {
    for (index = 0; index < userInfo.PlayerList.length; ++index) {
      tmpUserName = userInfo.PlayerList[index].Username;
      cmdText = `"InspectPlayer ` + userInfo.PlayerList[index].UniqueId + `"`;
      filename =  `InspectPlayer` + index + `.json`;
      const respData = await rconCmd (cmdText, filename);
      if (respData.length == 0) {
        //unknownCount = unknownCount + 1;
        //unknownUsers = unknownUsers + ' , ' + tmpUserName;
      } else if (respData.PlayerInfo) {
        const userRecord = new UserDetails();
        userRecord.playerName = respData.PlayerInfo.PlayerName;
        userRecord.uniqueId = respData.PlayerInfo.UniqueId;
        userRecord.kills = respData.PlayerInfo.KDA.split("/")[0];
        userRecord.deaths = respData.PlayerInfo.KDA.split("/")[1];
        userRecord.assists = respData.PlayerInfo.KDA.split("/")[2];
        userRecord.score = respData.PlayerInfo.Score;
        userRecord.cash = respData.PlayerInfo.Cash;
        userRecord.teamId = respData.PlayerInfo.TeamId;
        if (userRecord.teamId == 0){
          userRecord.teamColor = 'blue';
        } else {
          userRecord.teamColor = 'red';
        }
        //successCount = successCount + 1;
        //successUsers = successUsers + ' , ' + tmpUserName;
        usrListDetails.push(userRecord);
      } else {
        //failCount = failCount + 1;
        //failUsers = failUsers + ' , ' + tmpUserName;
      }
    }
  }
  //console.log('unknown user count: ' + unknownCount);
  //console.log('success user count: ' + successCount);
  //console.log('fail user count: ' + successCount);
  console.log('user detail list - array');
  console.log(usrListDetails);
  return usrListDetails;
}

async function userList(cmdType){
  class tmpItem {
    constructor() {
      this.optionInfo = {key: 'username', synonyms: ['User -1']}
      this.title = 'sample title'
    }
  }
  let usrList = new List({
    title: 'Select user number to ' + cmdType,
    description: '',
    items: {
    }
  });
  let cmdText = `"RefreshList"`;
  let filename = `refreshList.json`;
  const userInfo = await rconCmd (cmdText, filename);
  console.log(userInfo);
  if (userInfo.length == 0) {
    throw new Error("Sorry, RCON didn't respond in time, is there anything else I can help you with?");
  } else if (userInfo.PlayerList.length == 0) {
    throw new Error("RCON reports no players on server, is there anything else I can help you with?");
  } else {
    for (index = 0; index < userInfo.PlayerList.length; ++index) {
      tmpItemAry = new tmpItem;
      usrIdx = (index + 1);
      //debug to test 
      if (index == 0 ){
        userInfo.PlayerList[index].Username = 'zTestUserz';
      }
      tmpUserName = userInfo.PlayerList[index].Username;
      tmpItemAry.optionInfo = {key: cmdType + ' ' + userInfo.PlayerList[index].Username, synonyms: ['User ' + usrIdx]};
      //tmpItemAry.title = 'User ' + usrIdx + ' ' + userInfo.PlayerList[index].Username;
      tmpItemAry.title = usrIdx;
      tmpItemAry.description = userInfo.PlayerList[index].Username;
      usrList.inputValueData.listSelect.items.push(tmpItemAry);
    }
    usrList.user_count = usrIdx;
    console.log(usrList.inputValueData.listSelect.items)
    return usrList;
  } 
  throw new Error("Bruh, somethings wrong.  Peace out.");
};

let dateFormat = require('dateformat');
// instantiate dialog flow client
const app = dialogflow({debug: false});

// Handlers go here.
app.intent("Default Welcome Intent", conv => {
   conv.ask("Welcome to the Suncoast Pavlov Admin assistant, what would you like me to do?");
  });

// currently spawn an item to admin or all
// need to add spawn to team / individual
app.intent("spawnItem", async conv => {
  let respString = '';
  let respData = '';
  let index = 1;
  let spawnFailCount = 0;
  let spawnFailUsers = '';
  let spawnSuccessCount = 0;
  let spawnSuccessUsers = '';
  let spawnUnknownCount = 0;
  let spawnUnknownUsers = '';
  let spawnItem = conv.body.queryResult.parameters.item;
  let spawnUser = conv.body.queryResult.parameters.user;  // myself, admin, all
  switch(spawnUser) {
    case 'alpha': 
      uniqueId = config.rcon.alpha;
      break;
    case 'beta':
      uniqueId = config.rcon.beta;
      break;
    case 'charlie':
      uniqueId = config.rcon.charlie;
      break;
    default:
      uniqueId = config.rcon.defaultUniqueId;
    }
  let cmdText = `"GiveItem ` + uniqueId + ' ' + spawnItem + `"`;
  let filename =  "GiveItem" + index + '.json';
  console.log("Start Spawn Item intent");
  console.log('spawn item: ' + spawnItem);
  //console.log(conv);
  if (spawnUser == 'myself' || spawnUser == 'alpha' || spawnUser == 'beta' || spawnUser == 'charlie' ) {
    console.log('spawning to admin')
    respData = await rconCmd (cmdText, filename);
    console.log(respData)
    if (respData.length == 0) {
      respString = "Sorry, no response from the server, spawn may have failed.  ";
    } else if (respData.GiveItem) {
      console.log ('spawed')
      respString = spawnItem + ' spawned successfully, ';
    } else {
      console.log ('spawn failed')
      respString = ' sorry, spawn ' + spawnItem + ' failed, ';
    }
  } else if (spawnUser == 'all') {
    let cmdText = `"RefreshList"`;
    let filename = `refreshList.json`;
    const userInfo = await rconCmd (cmdText, filename);
    console.log(userInfo);
    if (userInfo.length == 0) {
      respString = "Sorry, Suncoast server didn't respond in time, ";
      console.log(respString);
    } else if (userInfo.PlayerList.length == 0) {
      respString = 'There are no active players at this time, ';
    } else if(userInfo.PlayerList.length == 1 ) {
      respString = 'There is 1 user playing. It is ' ;
      cmdText = `"GiveItem ` + userInfo.PlayerList[0].UniqueId + ' ' + spawnItem + `"`;
      filename =  "GiveItem" + index + '.json';
      respData = await rconCmd (cmdText, filename);
      if (respData.length == 0) {
        respString = "Sorry, no response from the server, spawn may have failed.  ";
      } else if (respData.GiveItem) {
        respString = spawnItem + ' spawned successfully, ';
      } else {
        respString = ' sorry, spawn ' + spawnItem + ' failed, ';
      }
      respString = respString + userInfo.PlayerList[0].Username;
      respString = respString + ', ';
    } else {
      for (index = 0; index < userInfo.PlayerList.length; ++index) {
        tmpUserName = userInfo.PlayerList[index].Username;
        cmdText = `"GiveItem ` + userInfo.PlayerList[index].UniqueId + ' ' + spawnItem + `"`;
        filename =  "GiveItem" + index + '.json';
        respData = await rconCmd (cmdText, filename);
        if (respData.length == 0) {
          spawnUnknownCount = spawnUnknownCount + 1;
          spawnUnknownUsers = spawnUnknownUsers + ' , ' + spawnFailUsers;
        } else if (respData.GiveItem) {
          spawnSuccessCount = spawnSuccessCount + 1;
          spawnSuccessUsers = spawnSuccessUsers + ' , ' + spawnSuccessUsers;
        } else {
          respString = ' sorry, spawn ' + spawnItem + ' failed, ';
          spawnFailCount = spawnFailCount + 1;
          spawnFailUsers = spawnFailUsers + ' , ' + spawnFailUsers
        }
        respString = respString + tmpUserName + ' , ';
        userLog = userInfo.PlayerList[index].Username + ' ' + userInfo.PlayerList[index].UniqueId + '\n';
      }
      respString = spawnItem + ' spawn status ';
      respString = respString + spawnSuccessCount + ' successful.';
      respString = respString + spawnFailCount + ' failed.';
      respString = respString + spawnUnknownCount + ' unknown.';
    }
  }
  respString = respString + ' what else can I help you with?';
  console.log(respString);
  conv.ask(respString);
});

// Indicates what users are on the sysem and logs it.
app.intent("listUsers", async conv => {
  let respString = '';
  let tmpUserName = '';
  let userLog = '';
  let cmdText = `"RefreshList"`;
  let filename = `refreshList.json`;
  console.log("Tripped getUsers intent");
  const userInfo = await rconCmd (cmdText, filename);
  console.log('build response');
  console.log(userInfo);
  if (userInfo.length == 0) {
    respString = "Sorry, Suncoast server didn't respond in time, ";
    console.log(respString);
  } else 
  {
    if (userInfo.PlayerList.length == 0) {
      respString = 'There are no active players at this time, ';
    }
    else {
      console.log('code to handle users') ;
      if(userInfo.PlayerList.length == 1 ) {
        respString = 'There is 1 user playing. It is ' ;
        respString = respString + userInfo.PlayerList[0].Username;
        respString = respString + ', ';
        userLog = userInfo.PlayerList[0].Username + ' ' + userInfo.PlayerList[0].UniqueId + '\n' ;
      } else {
        respString = 'There are ' + userInfo.PlayerList.length + ' users playing. They are ';
        for (let index = 0; index < userInfo.PlayerList.length; ++index) {
          tmpUserName = userInfo.PlayerList[index].Username;
          respString = respString + tmpUserName + ' , ';
          userLog = userInfo.PlayerList[index].Username + ' ' + userInfo.PlayerList[index].UniqueId;
        }
      }
    }
  }
  fs.writeFileSync('./tmp/userLog' + Date.now() + '.txt', userLog, function (err,data) {
    if (err) {
      return console.log(err);
    }
    console.log(data);
  });
respString = respString + 'is there anything else I can help you with?';
conv.ask(respString);
});

// return server information
app.intent("serverInfo", async conv => {
  let cmdText = `"ServerInfo"`;
  let filename = `serverInfo.json`;
  let respString = '';
  let gameMode = '';
  console.log("Tripped serverInfo intent");
  const servInf = await rconCmd (cmdText, filename);
  console.log('build response');
  console.log(servInf);
  if (servInf.length == 0) {
    respString = "Sorry, Suncoast server didn't respond in time, is there anything else I can help you with?";
    console.log(respString);
  } else 
  {
    let pos = servInf.ServerInfo.PlayerCount.indexOf("/");
    let playerCount = servInf.ServerInfo.PlayerCount.slice(0,pos);
    let maxPlayers = servInf.ServerInfo.PlayerCount.slice(pos+1,servInf.ServerInfo.PlayerCount.length);
    pos = servInf.ServerInfo.MapLabel.lastIndexOf("_");
    let map = servInf.ServerInfo.MapLabel.slice(pos+1,servInf.ServerInfo.MapLabel.length);
    switch(servInf.ServerInfo.GameMode) {
      case 'DM': 
        gameMode = 'Death Match'
        break;
      case 'TTT':
        gameMode = 'TTT Trouble in Terrorist Town'
        break;
      case 'TDM':
        gameMode = 'Team Death Match'
        break;
      case 'SND':
        gameMode = 'Search and Destroy'
        break;
      case 'ZWV':
        gameMode = 'Zombies'
        break;
      case 'GUN':
        gameMode = 'Gun Game'
        break;
      case 'WW2GUN':
        gameMode = 'World War 2 Gun Game'
        break;
      case 'TANKTDM':
        gameMode = 'Tank Team Deathmatch'
        break;
      case 'KOTH':
        gameMode = 'Koth'
        break;
      default:
        gameMode = 'Custom'
      }
      respString = 'Suncoast server is set to ' + map + ' map in ' + gameMode + ' game mode. ';
      if (playerCount == 0) {
        respString = respString + 'The server is currently empty.';
      }
      else {
        respString = respString + 'There are ' + playerCount + ' out of ' + maxPlayers + ' players.';
      }
  }
  conv.ask(respString);
});

// switch map
app.intent("switchMap", async conv => {
  let respString = '';
  let targetMap = conv.body.queryResult.parameters.map;
  let targetMode = conv.body.queryResult.parameters.MapMode;
  let cmdText = `"SwitchMap ` + targetMap + ' ' + targetMode + `"`;
  let filename = 'switchMap.json';
  const switchMapResp = await rconCmd (cmdText, filename);
  if (switchMapResp.length == 0) {
    respString = 'Server not responding, not sure if map switch is successful.';
  } else {
    if (switchMapResp.SwitchMap) {
      respString = "Map switching to " + targetMap + ' ' + targetMode;
    }
    else {
      respString = 'Map switch failed. ';
    }
  }

  respString = respString + '  Anything else I can do for you?';
  console.log(respString);
  conv.ask(respString);
});

app.intent("blacklist", async conv => {
  let respString = '';
  let cmdText = `"Blacklist"`;
  let filename = `blacklist.json`;
  console.log("Start blacklist intent");
  const blacklistResp = await rconCmd (cmdText, filename);
  console.log('blacklistResp: ');
  console.log(blacklistResp);
  if (blacklistResp.length == 0) {
    respString = 'Server not responding, not sure if map rotation is successful.';
  } else if (blacklistResp.BlackList.length == 0) {
      respString = 'There are no banned players at this time, ';
  } else {
    console.log('code to handle users') ;
    if(blacklistResp.BlackList.length == 1 ) {
        respString = 'There is 1 user banned. It is ' ;
        respString = respString + blacklistResp.BlackList[0];
        respString = respString + ', ';
      } else {
        respString = 'There are ' + blacklistResp.BlackList.length + ' users banned. They are ';
        for (let index = 0; index < blacklistResp.BlackList.length; ++index) {
          respString = respString + blacklistResp.BlackList[index] + ' , ';
        }
      }
    }
  respString = respString + '  Anything else I can do for you?';
  console.log(respString);
  conv.ask(respString);
});

app.intent("rotateMap", async conv => {
  let respString = '';
  let cmdText = `"RotateMap"`;
  let filename = `rotateMap.json`;
  console.log("Start rotate map intent");
  const rotateMapResp = await rconCmd (cmdText, filename);
  console.log('rotateMapResp: ');
  console.log(rotateMapResp);
  if (rotateMapResp.length == 0) {
    respString = 'Server not responding, not sure if map rotation is successful.';
  } else {
    if (rotateMapResp.RotateMap) {
      respString = "Map rotating";
    }
    else {
      respString = 'Map rotate failed. ';
    }
  }

  respString = respString + '  Anything else I can do for you?';
  console.log(respString);
  conv.ask(respString);
});

app.intent("resetSND", async conv => {
  let respString = '';
  let cmdText = `"ResetSND"`;
  let filename = `resetSND.json`;
  console.log("Start resetSND intent");
  const resetSNDResp = await rconCmd (cmdText, filename);
  console.log('resetSND: ');
  console.log(resetSNDResp);
  if (resetSNDResp.length == 0) {
    respString = 'Server not responding, not sure if map rotation is successful.';
  } else { if (resetSNDResp.ResetSND) {
    respString = "Game resetting";
  }
    else {
      respString = 'Game reset failed. ';
    }
  }

  respString = respString + '  Anything else I can do for you?';
  console.log(respString);
  conv.ask(respString);
});

// Set admin or all skin
// need to add team / user set skin
app.intent("setSkin", async conv => {
  let respString = '';
  let respData = '';
  let index = 1;
  let setSkinFailCount = 0;
  let setSkinFailUsers = '';
  let setSkinSuccessCount = 0;
  let setSkinSuccessUsers = '';
  let setSkinUnknownCount = 0;
  let setSkinUnknownUsers = '';
  let skinType = conv.body.queryResult.parameters.skinType;
  let skinUser = conv.body.queryResult.parameters.user;  // myself, admin, all, alpha, beta, charlie
  switch(skinUser) {
    case 'alpha': 
      uniqueId = config.rcon.alpha;
      break;
    case 'beta':
      uniqueId = config.rcon.beta;
      break;
    case 'charlie':
      uniqueId = config.rcon.charlie;
      break;
    default:
      uniqueId = config.rcon.defaultUniqueId;
    }
  let cmdText = `"SetPlayerSkin ` + uniqueId + ' ' + skinType + `"`;
  let filename =  "setPlayerSkin" + index + '.json';
  console.log("Start setSkin intent");
  console.log('set skin: ' + skinType);
  //console.log(conv);
  if (skinUser == 'myself' || skinUser == 'alpha' || skinUser == 'beta' || skinUser == 'charlie' ) {
    console.log('set skin for ' + skinUser)
    respData = await rconCmd (cmdText, filename);
    console.log(respData)
    if (respData.length == 0) {
      respString = "Sorry, no response from the server, spawn may have failed.  ";
    } else if (respData.SetPlayerSkin) {
      console.log ('skin set')
      respString = skinType + ' set successfully, ';
    } else {
      console.log ('Set skin failed')
      respString = ' Sorry, set ' + skinType + ' failed, ';
    }
  } else if (skinUser == 'all') {
    let cmdText = `"RefreshList"`;
    let filename = `refreshList.json`;
    const userInfo = await rconCmd (cmdText, filename);
    console.log(userInfo);
    if (userInfo.length == 0) {
      respString = "Sorry, Suncoast server didn't respond in time, ";
    } else if (userInfo.PlayerList.length == 0) {
      respString = 'There are no active players at this time, ';
    } else if(userInfo.PlayerList.length == 1 ) {
      cmdText = `"SetPlayerSkin ` + userInfo.PlayerList[0].UniqueId + ' ' + skinType + `"`;
      filename =  "setPlayerSkin" + index + '.json';
      respData = await rconCmd (cmdText, filename);
      if (respData.length == 0) {
        respString = "Sorry, no response from the server, spawn may have failed.  ";
      } else if (respData.SetPlayerSkin) {
        respString = skinType + ' set successfully, ';
      } else {
        respString = ' sorry, set skin ' + skinType + ' failed, ';
      }
      respString = respString + userInfo.PlayerList[0].Username;
      respString = respString + ', ';
    } else {
      for (index = 0; index < userInfo.PlayerList.length; ++index) {
        tmpUserName = userInfo.PlayerList[index].Username;
        cmdText = `"SetPlayerSkin ` + userInfo.PlayerList[index].UniqueId + ' ' + skinType + `"`;
        filename =  "setPlayerSkin" + index + '.json';
        respData = await rconCmd (cmdText, filename);
        if (respData.length == 0) {
          setSkinUnknownCount = setSkinUnknownCount + 1;
          setSkinUnknownUsers = setSkinUnknownUsers + ' , ' + setSkinFailUsers;
        } else if (respData.SetPlayerSkin) {
          setSkinSuccessCount = setSkinSuccessCount + 1;
          setSkinSuccessUsers = setSkinSuccessUsers + ' , ' + setSkinSuccessUsers;
        } else {
          respString = ' sorry, set skin ' + skinType + ' failed, ';
          setSkinFailCount = setSkinFailCount + 1;
          setSkinFailUsers = setSkinFailUsers + ' , ' + setSkinFailUsers
        }
        respString = respString + tmpUserName + ' , ';
        userLog = userInfo.PlayerList[index].Username + ' ' + userInfo.PlayerList[index].UniqueId + '\n';
      }
      respString = skinType + ' Set Skin status ';
      respString = respString + setSkinSuccessCount + ' successful.';
      respString = respString + setSkinFailCount + ' failed.';
      respString = respString + setSkinUnknownCount + ' unknown.';
    }
  }
  respString = respString + ' what else can I help you with?';
  console.log(respString);
  conv.ask(respString);
});

app.intent("kick", async (conv) => {
  let createCard = false;
  const lifespan = 5;
  const intentTime = Date.now();
  formattedDate = dateFormat(intentTime, "dddd, mmmm dS, yyyy, h:MM:ss TT");
  const contextParameters = {
    origin: 'kick',
    level: '0'
  };
  conv.contexts.set("kick", lifespan, contextParameters); 
  console.log('entered kick intent')
  try {
    const usrList = await userList('Kick');
    console.log('In new kick intent')
    console.log(usrList);
    conv.ask('Select user to kick')
    conv.ask(usrList);
    console.log('exit kick intent')
  } catch(e) {
    console.log(e);
    conv.ask(e);
  }
});

app.intent("ban", async (conv) => {
  let createCard = false;
  const lifespan = 5;
  const intentTime = Date.now();
  formattedDate = dateFormat(intentTime, "dddd, mmmm dS, yyyy, h:MM:ss TT");
  const contextParameters = {
    origin: 'ban',
    level: '0'
  };
  conv.contexts.set("ban", lifespan, contextParameters); 
  console.log('entered ban intent');
  try {
    const usrList = await userList('Ban');
    console.log('In new ban intent')
    console.log(usrList);
    conv.ask('Select user to ban')
    conv.ask(usrList);
    console.log('exit ban intent')
  } catch(e) {
    console.log(e);
    conv.ask(e);
  }});

app.intent("kill", async (conv) => {
  let createCard = false;
  const lifespan = 5;
  const intentTime = Date.now();
  formattedDate = dateFormat(intentTime, "dddd, mmmm dS, yyyy, h:MM:ss TT");
  const contextParameters = {
    origin: 'kill',
    level: '0'
  };
  conv.contexts.set("kill", lifespan, contextParameters); 
  console.log('entered kill intent');
  try {
    const usrList = await userList('Kill');
    console.log('In new kill intent')
    console.log(usrList);
    conv.ask('Select user to kill')
    conv.ask(usrList);
    console.log('exit kill intent')
  } catch(e) {
    console.log(e);
    conv.ask(e);
  }});

app.intent("actions_intent_OPTION", async (conv, params, option) => {
  let cmdText = `"` + option + `"`;
  let cmdType = option.split(" ")[0];
  let filename = cmdType + '.json';
  console.log("Start " + cmdText + " command execution");
  const respData = await rconCmd (cmdText, filename);
  let respString = '';
  console.log('RCON command: ' + cmdText);
  console.log(respData)
  if (respData.length == 0) {
    respString = 'Sorry, RCON not responding, is there anything else I can help you with?'
  } else if (respData.Kick || respData.Ban || respData.Kill) {
    respString = option + ' successful.  Is there anything else I can do for you?';
  } else {
    respString = ' Sorry, RCON ' + cmdType + ' of ' + option + ' failed.  Is there anything else I can help you with?';
  }

  conv.ask(respString)

});


// Set admin or all skin
// need to add team / user set skin
app.intent("setTeamSkin", async conv => {
  let respString = '';
  let respData = '';
  let usrDetailList = {};
  let index = 0;
  let skinType = conv.body.queryResult.parameters.skin;
  console.log (conv.body.queryResult.parameters);
  let teamColor = conv.body.queryResult.parameters.team;
  console.log("Start set team skin intent");
  try {
    const usrDetailList = await userListDetails();
    console.log(usrDetailList);
    conv.ask('check the logs, user details written, what else can I help you with?');
    if(usrDetailList.length == 0) {
      respString = "Sorry, RCON did not return a player list"
    } else {
      console.log(usrDetailList.length)
      for (let index = 0; index < usrDetailList.length; index++) {
        if (usrDetailList[index].teamColor == teamColor) {
          cmdText = `"SetPlayerSkin ` + usrDetailList[index].uniqueId + ' ' + skinType + `"`;
          filename =  "setPlayerSkin" + index + '.json';
          respData = await rconCmd (cmdText, filename);
          if (respData.length == 0) {
            //setSkinUnknownCount = setSkinUnknownCount + 1;
            //setSkinUnknownUsers = setSkinUnknownUsers + ' , ' + usrDetailList[index].UniqueId;
          } else if (respData.SetPlayerSkin) {
            //setSkinSuccessCount = setSkinSuccessCount + 1;
            //setSkinSuccessUsers = setSkinSuccessUsers + ' , ' + usrDetailList[index].UniqueId;
          } else {
            respString = ' sorry, set skin ' + skinType + ' failed, ';
            //setSkinFailCount = setSkinFailCount + 1;
            //setSkinFailUsers = setSkinFailUsers + ' , ' + usrDetailList[index].UniqueId
          }
          respString = respString + usrDetailList[index].uniqueId + ' , ';
        }
      }
      respString = skinType + ' Set Skin status ';
      //respString = respString + setSkinSuccessCount + ' successful.';
      //respString = respString + setSkinFailCount + ' failed.';
      //respString = respString + setSkinUnknownCount + ' unknown.';
    }
  }
  catch(e) {
    console.log(e);
    conv.ask(e);
  }
});

app.intent("scoreboard", async conv => {
  let respString = '';
  let respData = '';
  let usrDetailList = {};
  console.log("Start scoreboard intent");
  try {
    const usrDetailList = await userListDetails();
    console.log('In new scoreboard intent')
    console.log(usrDetailList);
    conv.ask('check the logs, user details written, what else can I help you with?');
  } catch(e) {
    console.log(e);
    conv.ask(e);
  }
});

// export app
module.exports = app;